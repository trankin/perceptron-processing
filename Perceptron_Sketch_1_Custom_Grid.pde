



Perceptron ptron;
PImage img;
PFont f;
// 2,000 training points
Trainer[] training = new Trainer[20000];

int count = 0;
float screenWidth = 622;
float screenHeight = 622;
color fillColor = color(0);
float gridWidth = 20;
float gridHeight = 20;

//[full] The formula for a line
float f(float x) {
  return 2.5 * x + 4;
}
//[end]

float reMapX(float val)
{
  return map(val, -1 * gridWidth / 2, gridWidth / 2, -1 * (screenWidth / 2), screenWidth / 2);
}

float reMapY(float val)
{
  return -1 * map(val, -1 * gridWidth / 2, gridWidth / 2, -1 * (height / 2), height / 2);
}

void setup() {
  size(922, 622);
  f = createFont("Arial",16,true); // Arial, 16 point, anti-aliasing on
  textFont(f,16);
  
  img = loadImage("export.png");
  ptron = new Perceptron(3);

  // Make 2,000 training points.
  for (int i = 0; i < training.length; i++) {
    float x = random(-gridWidth/2,gridWidth/2);
    float y = random(-gridHeight/2,gridHeight/2);
    //[full] Is the correct answer 1 or -1?
    int answer = 1;
    if (y < f(x)) answer = -1;
    //[end]
    training[i] = new Trainer(x, y, answer);
  }
  
  smooth();
}


void plotFunction()
{
  smooth();
  strokeWeight(3); 
  stroke(0, 0, 255);
  for(float i = -10; i < 11; i += .01)
  {
      point(reMapX(i), reMapY(f(i)));
  }
}



void draw() {

  if(count < training.length - 1)
  {
    background(255);
    // Draw the Grid
    image(img, 0, 0);
    float xMin = (gridWidth / 2) * -1;
    float xMax = gridWidth / 2;
    
    // Re-Orient 0,0
    translate(screenWidth/2,screenHeight/2);
    


    for(int i = 0; i < 50; i++)
    {
      ptron.train(training[count].inputs, training[count].answer);
      count = (count + 1) % training.length;
    }
    
    for (int i = 0; i < count; i++) {
      
      int guess = ptron.feedforward(training[i].inputs);
      
      if (guess > 0) {
        // greenish
        fillColor = color(0, 210, 75, 150);
      }
      else {
        // orangeish
        fillColor = color(255, 130, 0, 150);
      }
      
      fill(fillColor);
      stroke(fillColor);
      
      //[end]
      ellipse(reMapX(training[i].inputs[0]), reMapY(training[i].inputs[1]), 8, 8);
    }
      
    
    // Draw our function f line in blue
    
    strokeWeight(3); 
    stroke(0, 0, 255);
    line(reMapX(xMin), reMapY(f(xMin)), reMapX(xMax), reMapY(f(xMax)));
    
    
    //plotFunction();
    
      // Draw the line based on the current weights
    // Formula is weights[0]*x + weights[1]*y + weights[2] = 0
    stroke(255, 0, 255);
    strokeWeight(1);
    float[] weights = ptron.getWeights();
    
    float x1 = xMin;
    float x2 = xMax;
    
    float y1 = (-weights[2] - weights[0] * x1) / weights[1];
    float y2 = (-weights[2] - weights[0] * x2) / weights[1];
    
    
    line(reMapX(x1), reMapY(y1), reMapX(x2), reMapY(y2));
    
    
    float m = (y2-y1) / (x2-x1);
    float b = (y2 - m * x2);
    
    fill(255, 0, 255);
    text("f(x) = " + nf(m, 1, 2) + "x + " + nf(b, 1, 2), 321, -280);
    
    
    m = (f(-3) - f(3)) / (-3 - 3);
    b = (f(-3) - m * -3);
    
    fill(0, 0, 255);
    text("f(x) = " + nf(m, 1, 2) + "x + " + nf(b, 1, 2), 321, -260);
    
    fill(0);
    text(count, 321, -240);
    
    if(count % 50 == 0)
    {
        save("output/output_" + nf(count, 6, 0) + ".png"); 
    }

  }

}